<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <style>
        body {
            padding-top: 50px;
            padding-bottom: 20px;
        }
    </style>

    <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
    <![endif]-->
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Code test</a>
        </div>

    </div>
</nav>

<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="jumbotron">
    <div class="container">
        <h1>Harry Torry</h1>
    </div>
</div>

<div class="container">
    <!-- Example row of columns -->
    <div class="row">
        <div class="col-md-12">
            @if($currentCategory->parent == 0)
                This is the root level
            @else
                Parent: <a href="{{ $currentCategory->parentUrl() }}">{{ $currentCategory->parentName() }}</a>
            @endif
        </div>
        <div class="col-md-6">
            <h2>Categories</h2>

            <ul>
            @foreach ($categories as $category)
                    <li><a href="{{ $category->id }}">{{ $category->title }}</a></li>
            @endforeach
            </ul>
        </div>
        <div class="col-md-6">
            <h2>Products</h2>

            <ul>
            @foreach ($products as $product)
                    <li>{{ $product->title }}</li>
            @endforeach
            </ul>
        </div>
    </div>

    <hr>

    <footer>
        <p>&copy; Company 2015</p>
    </footer>
</div> <!-- /container -->
<script src="https://www.baqend.com/js-sdk/latest/baqend.min.js"></script>
<script
        src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha256-k2WSCIexGzOj3Euiig+TlR8gA0EmPjuc79OEeY5L45g="
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</body>
</html>