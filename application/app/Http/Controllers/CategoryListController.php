<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryListController extends Controller
{
    public function index(Category $category)
    {
        return view('category.list')
            ->with('currentCategory', $category)
            ->with('categories', $category->children())
            ->with('products', $category->products);
    }
}
