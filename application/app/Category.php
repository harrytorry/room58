<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $fillable = [
        'parent',
        'title'
    ];

    public function children()
    {
        $id = $this->id ?: 0;

        return Category::where('parent', '=', $id)->get();
    }

    public function parentUrl()
    {
        if ($this->parent == 0) {
            return;
        }

        return route('categories.list', ['category' => $this->parent]);
    }

    public function parentName()
    {
        if ($this->parent == 0) {
            return;
        }

        $category = Category::findOrFail($this->parent);
        return $category->title;
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
