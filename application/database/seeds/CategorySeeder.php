<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->makeRootCategories();
        $this->makeSecondLevelCatagories();
        $this->makeThirdLevelCategories();
        $this->makeFourthLevelCategories();
    }

    private function makeRootCategories()
    {
        for ($i = 0; $i < 5; $i++) {
            $faker = Faker\Factory::create();

            $category = new \App\Category([
                'title' => $faker->safeColorName,
                'parent' => 0
            ]);
            $category->save();
        }
    }

    private function makeSecondLevelCatagories()
    {
        for ($i = 0; $i < 25; $i++) {
            $faker = Faker\Factory::create();

            $category = new \App\Category([
                'title' => $faker->safeColorName,
                'parent' => $faker->numberBetween(0, 4)
            ]);
            $category->save();
        }
    }

    private function makeThirdLevelCategories()
    {
        for ($i = 0; $i < 50; $i++) {
            $faker = Faker\Factory::create();

            $category = new \App\Category([
                'title' => $faker->safeColorName,
                'parent' => $faker->numberBetween(5, 30)
            ]);
            $category->save();
        }
    }

    private function makeFourthLevelCategories()
    {
        for ($i = 0; $i < 200; $i++) {
            $faker = Faker\Factory::create();

            $category = new \App\Category([
                'title' => $faker->safeColorName,
                'parent' => $faker->numberBetween(31, 80)
            ]);
            $category->save();
        }
    }
}
