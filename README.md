# Code test
Harry Torry

## Setup


### with docker

* add `127.0.0.1 room58.jobtest.dev` to `etc/hosts`
* run `docker-compose up -d` to initialise all of the projects
* run `docker-compose run --rm composer install` to install dependencies
* run `docker-compose run --rm artisan migrate` to set up the database
* run `docker-compose run --rm db:seed` to seed the database


### Using the app

You can go to `http://room58.jobtest.dev:85/` to see the site running

